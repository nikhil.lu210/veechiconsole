<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// Route::get('/home', [
//     'uses' => 'HomeController@index',
//     'as' => 'home'
// ]);

Route::get('/home', 'HomeController@index')->name('home');


Route::group([

    'middleware' => 'auth' 
 
], function() {

    Route::get('/setting', [
        'uses' => 'HomeController@showChangePasswordForm',
        'as' => 'setting'
    ]);

    Route::post('/setting/update', [
        'uses' => 'Auth\ChangePasswordController@changePassword',
        'as' => 'setting.change.password'
    ]);

    // Route::get('/changePassword','HomeController@showChangePasswordForm');

    /*===============================================
    ==================== Records ====================
    ===============================================*/

    // +++++++++++++++++ All Website +++++++++++++++++
    Route::get('/allwebsite', [
        'uses' => 'Record\AllWebsiteController@index',
        'as' => 'record.allwebsite.index'
    ]);
    Route::get('/allwebsite/create', [
        'uses' => 'Record\AllWebsiteController@create',
        'as' => 'record.allwebsite.create'
    ]);
    Route::post('/allwebsite/store', [
        'uses' => 'Record\AllWebsiteController@store',
        'as' => 'record.allwebsite.store'
    ]);
    Route::get('/allwebsite/show/{id}', [
        'uses' => 'Record\AllWebsiteController@show',
        'as' => 'record.allwebsite.show'
    ]);
    Route::post('/allwebsite/update/{id}', [
        'uses' => 'Record\AllWebsiteController@update',
        'as' => 'record.allwebsite.update'
    ]);
    Route::get('/allwebsite/destroy/{id}',[
        'uses'=>'Record\AllWebsiteController@destroy',
        'as'=>'record.allwebsite.destroy'
    ]);

    // +++++++++++++++++ All Customer ++++++++++++++++
    Route::get('/allcustomer', [
        'uses' => 'Record\AllCustomerController@index',
        'as' => 'record.allcustomer.index'
    ]);
    Route::get('/allcustomer/create', [
        'uses' => 'Record\AllCustomerController@create',
        'as' => 'record.allcustomer.create'
    ]);
    Route::post('/allcustomer/store', [
        'uses' => 'Record\AllCustomerController@store',
        'as' => 'record.allcustomer.store'
    ]);
    Route::get('/allcustomer/show/{id}', [
        'uses' => 'Record\AllCustomerController@show',
        'as' => 'record.allcustomer.show'
    ]);
    Route::post('/allcustomer/update/{id}', [
        'uses' => 'Record\AllCustomerController@update',
        'as' => 'record.allcustomer.update'
    ]);
    Route::get('/allcustomer/destroy/{id}',[
        'uses'=>'Record\AllCustomerController@destroy',
        'as'=>'record.allcustomer.destroy'
    ]);


    /* ==============================================
    =================== Settings ====================
    ===============================================*/
    
    // +++++++++++++++++ Website Type +++++++++++++++++
    Route::get('/website_type', [
        'uses' => 'Setting\WebsiteTypeController@index',
        'as' => 'setting.website_type.index'
    ]);
    Route::get('/website_type/create', [
        'uses' => 'Setting\WebsiteTypeController@create',
        'as' => 'setting.website_type.create'
    ]);
    Route::post('/website_type/store', [
        'uses' => 'Setting\WebsiteTypeController@store',
        'as' => 'setting.website_type.store'
    ]);
    Route::get('/website_type/show/{id}', [
        'uses' => 'Setting\WebsiteTypeController@show',
        'as' => 'setting.website_type.show'
    ]);
    Route::post('/website_type/update/{id}', [
        'uses' => 'Setting\WebsiteTypeController@update',
        'as' => 'setting.website_type.update'
    ]);
    Route::get('/website_type/destroy/{id}',[
        'uses'=>'Setting\WebsiteTypeController@destroy',
        'as'=>'setting.website_type.destroy'
    ]);


    // +++++++++++++++++ Packages +++++++++++++++++
    Route::get('/package', [
        'uses' => 'Setting\PackageController@index',
        'as' => 'setting.package.index'
    ]);
    Route::get('/package/create', [
        'uses' => 'Setting\PackageController@create',
        'as' => 'setting.package.create'
    ]);
    Route::post('/package/store', [
        'uses' => 'Setting\PackageController@store',
        'as' => 'setting.package.store'
    ]);
    Route::get('/package/show/{id}', [
        'uses' => 'Setting\PackageController@show',
        'as' => 'setting.package.show'
    ]);
    Route::post('/package/update/{id}', [
        'uses' => 'Setting\PackageController@update',
        'as' => 'setting.package.update'
    ]);
    Route::get('/package/destroy/{id}',[
        'uses'=>'Setting\PackageController@destroy',
        'as'=>'setting.package.destroy'
    ]);


    // +++++++++++++++++ Platforms +++++++++++++++++
    Route::get('/platform', [
        'uses' => 'Setting\PlatformController@index',
        'as' => 'setting.platform.index'
    ]);
    Route::get('/platform/create', [
        'uses' => 'Setting\PlatformController@create',
        'as' => 'setting.platform.create'
    ]);
    Route::post('/platform/store', [
        'uses' => 'Setting\PlatformController@store',
        'as' => 'setting.platform.store'
    ]);
    Route::get('/platform/show/{id}', [
        'uses' => 'Setting\PlatformController@show',
        'as' => 'setting.platform.show'
    ]);
    Route::post('/platform/update/{id}', [
        'uses' => 'Setting\PlatformController@update',
        'as' => 'setting.platform.update'
    ]);
    Route::get('/platform/destroy/{id}',[
        'uses'=>'Setting\PlatformController@destroy',
        'as'=>'setting.platform.destroy'
    ]);


    // +++++++++++++++++ Domains +++++++++++++++++
    Route::get('/domain', [
        'uses' => 'Setting\DomainController@index',
        'as' => 'setting.domain.index'
    ]);
    Route::get('/domain/create', [
        'uses' => 'Setting\DomainController@create',
        'as' => 'setting.domain.create'
    ]);
    Route::post('/domain/store', [
        'uses' => 'Setting\DomainController@store',
        'as' => 'setting.domain.store'
    ]);
    Route::get('/domain/show/{id}', [
        'uses' => 'Setting\DomainController@show',
        'as' => 'setting.domain.show'
    ]);
    Route::post('/domain/update/{id}', [
        'uses' => 'Setting\DomainController@update',
        'as' => 'setting.domain.update'
    ]);
    Route::get('/domain/destroy/{id}',[
        'uses'=>'Setting\DomainController@destroy',
        'as'=>'setting.domain.destroy'
    ]);


    // +++++++++++++++++ Hostings +++++++++++++++++
    Route::get('/hosting', [
        'uses' => 'Setting\HostingController@index',
        'as' => 'setting.hosting.index'
    ]);
    Route::get('/hosting/create', [
        'uses' => 'Setting\HostingController@create',
        'as' => 'setting.hosting.create'
    ]);
    Route::post('/hosting/store', [
        'uses' => 'Setting\HostingController@store',
        'as' => 'setting.hosting.store'
    ]);
    Route::get('/hosting/show/{id}', [
        'uses' => 'Setting\HostingController@show',
        'as' => 'setting.hosting.show'
    ]);
    Route::post('/hosting/update/{id}', [
        'uses' => 'Setting\HostingController@update',
        'as' => 'setting.hosting.update'
    ]);
    Route::get('/hosting/destroy/{id}',[
        'uses'=>'Setting\HostingController@destroy',
        'as'=>'setting.hosting.destroy'
    ]);
 });








