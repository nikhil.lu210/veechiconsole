<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Veechi Admin | LOGIN</title>
    
    {{--  Font-Awesome CSS  --}}
    <link rel="stylesheet" href="{{ asset('files/css/font-awesome/all.min.css') }}">

    {{--  Bootstrap CSS  --}}
    <link rel="stylesheet" href="{{ asset('files/css/bootstrap/bootstrap.min.css') }}">

    {{-- Main CSS --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('files/css/main.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('files/css/style.css') }}">
    <style>
        .material-half-bg .cover {
            background-color: #ed1b24;
        }
        .forget-pass{
            position: absolute;
            right: 40px;
            bottom: 168px;
        }
        .login-head{
            color: #e91b24;
        }
        a, a:hover {
            color: #eb1b24;
            text-decoration: none;
        }
        .btn-primary, .btn-primary:hover, .btn-primary:not(:disabled):not(.disabled):active, .btn-primary:not(:disabled):not(.disabled).active, .show > .btn-primary.dropdown-toggle{
            background-color: #eb1b24;
            border-color: #e91b24;
        }
        .form-control{
            transition: 0.5s all ease-in-out;
        }
        .form-control:focus{
            border-color: #e91b24;
            transition: 0.5s all ease-in-out;
        }
        .login-content{
          background: #e7e7e7; /* Old browsers */
          background: -moz-linear-gradient(bottom, #e7e7e7 48%, #e7e7e7 49%, #e7e7e7 49%, #e7e7e7 50%, #f16f5c 50%, #f6290c 50%, #f02f17 50%, #ed1b24 100%, #e7e7e7 100%); /* FF3.6-15 */
          background: -webkit-linear-gradient(bottom, #e7e7e7 48%,#e7e7e7 49%,#e7e7e7 49%,#e7e7e7 50%,#f16f5c 50%,#f6290c 50%,#f02f17 50%,#ed1b24 100%,#e7e7e7 100%); /* Chrome10-25,Safari5.1-6 */
          background: linear-gradient(to top, #e7e7e7 48%,#e7e7e7 49%,#e7e7e7 49%,#e7e7e7 50%,#f16f5c 50%,#f6290c 50%,#f02f17 50%,#ed1b24 100%,#e7e7e7 100%);
        }
    </style>
  </head>
  <body>
    <section class="material-half-bg">
      <div class="cover"></div>
    </section>
    <section class="login-content">
      <div class="logo">
        <h1>Veechi Technology</h1>
      </div>
      <div class="login-box">
        <form class="login-form" method="POST" action="{{ route('login') }}">
        @csrf
          <h3 class="login-head"><i class="far fa-user"></i> SIGN IN</h3>
          <div class="form-group">
            <label class="control-label">EMAIL</label>
            <input class="form-control"  id="email" type="email" class="{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

            @if ($errors->has('email'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
          </div>
          <div class="form-group">
            <label class="control-label">PASSWORD <p class="semibold-text mb-2 forget-pass"><a href="#" data-toggle="flip">Forgot Password ?</a></p></label>
            <input class="form-control" type="password" class="{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

            @if ($errors->has('password'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
          </div>
          <div class="form-group btn-container">
            <button class="btn btn-primary btn-block" type="submit"><i class="fas fa-fingerprint"></i> SIGN IN</button>
          </div>
        </form>
        <form class="forget-form" method="POST" action="{{ route('password.email') }}">
          @csrf
          <h3 class="login-head"><i class="fas fa-key"></i> Forgot Password ?</h3>
          <div class="form-group">
              <label class="control-label">EMAIL</label>
              <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
              @if ($errors->has('email'))
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('email') }}</strong>
                  </span>
              @endif
          </div>
          <div class="form-group btn-container">
            <button class="btn btn-primary btn-block"><i class="fa fa-unlock fa-lg fa-fw"></i>RESET</button>
          </div>
          <div class="form-group mt-3">
            <p class="semibold-text mb-0"><a href="#" data-toggle="flip"><i class="fa fa-angle-left fa-fw"></i> Back to Login</a></p>
          </div>
        </form>
      </div>
    </section>
    
    

    {{--  Jquery JS Library  --}}
    <script src="{{ asset('files/js/jquery/jquery-3.3.1.min.js') }}" crossorigin="anonymous"></script>
    <script src="{{ asset('files/js/jquery/popper.min.js') }}" defer></script>

    {{--  Bootstrap JS  --}}
    <script src="{{ asset('files/js/bootstrap/bootstrap.min.js') }}" defer></script>

    {{-- Vali Admin JS --}}
    <script src="{{ asset('files/js/vali/main.js') }}" defer></script>
    <script src="{{ asset('files/js/vali/plugins/pace.min.js') }}" defer></script>

    {{--  Custom JS  --}}
    <script src="{{ asset('files/js/script.js')}}"></script>
    <script type="text/javascript">
        // Login Page Flipbox control
        $('.login-content [data-toggle="flip"]').click(function() {
            $('.login-box').toggleClass('flipped');
            return false;
        });
    </script>
  </body>
</html>