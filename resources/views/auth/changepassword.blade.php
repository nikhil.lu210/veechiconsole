@extends('layouts.main')

@section('page_title', '| Package Create')

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        label{
            font-weight: 400;
            color: #616161;
        }
        .form-control {
            height: calc(2.25rem + 2px);
            padding: 7px 10px;
            font-weight: 500;
            color: #222222;
            border: 1px solid #dddddd;
            border-radius: .25rem;
            box-shadow: none !important;
            transition: 0.5s all ease-in-out;
        }
        .form-control:focus{
            border-color: #ed1b24;
            transition: 0.5s all ease-in-out;
        }
    </style>
@endsection

@section('content')
    {{--  Body Contents Here  --}}
    
    {{-- ========< Breadcrumb Part Starts >======== --}}
    <section class="breadcrumb-part">
        <div class="app-title">
            <div>
                <h1><i class="far fa-folder-open"></i> <b>Change Password</b></h1>
            </div>
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fas fa-home fa-lg"></i></a></li>
                <li class="breadcrumb-item active">Setting</li>
            </ul>
        </div>
    </section>
    {{-- =========< Breadcrumb Part Ends >========= --}}


    {{-- ========< Change Password Part Starts >======== --}}
    <section class="pass-change-part">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="float-left">Change Password</h3>
                            <a href="javascript:history.back()" class="btn btn-light btn-sm btn-create m-l-10" data-toggle="tooltip" data-placement="top" title="Back To Previous Page">Back</a>
                        </div>

                        <div class="card-body">
                            @if (session('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                            @endif @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                            @endif

                            <form class="form-horizontal" method="POST" action="{{ route('setting.change.password') }}">
                            {{ csrf_field() }}
                            <div class="form-group{{ $errors->has('current-password') ? ' has-error' : '' }}">
                                <div class="col-md-12">
                                    <label for="new-password" class="control-label">Current Password</label>
                                    <input id="current-password" type="password" class="form-control" name="current-password" placeholder="Current Password" required> @if ($errors->has('current-password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('current-password') }}</strong>
                                    </span> @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('new-password') ? ' has-error' : '' }}">
                                <div class="col-md-12">
                                    <label for="new-password" class="control-label">New Password</label>
                                    <input id="new-password" type="password" class="form-control" name="new-password" placeholder="New Password" required> @if ($errors->has('new-password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('new-password') }}</strong>
                                    </span> @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for="new-password-confirm" class="control-label">Confirm New Password</label>
                                    <input id="new-password-confirm" type="password" class="form-control" name="new-password_confirmation" placeholder="Re-type New Password" required>
                                </div>
                            </div>
                        </div>

                        <div class="card-footer">
                            <button class="btn btn-light btn-sm btn-create m-r-25" type="submit" data-toggle="tooltip" data-placement="top" title="Change Password">Change Password</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{-- =========< Change Password Part Ends >========= --}}
    
@endsection

@section('scripts')
    {{--  External Javascript  --}}
    
@endsection