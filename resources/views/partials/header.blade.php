<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

{{--  <!-- CSRF Token -->  --}}
<meta name="csrf-token" content="{{ csrf_token() }}">

{{--  Page Title  --}}
<title> Veechi Console @yield('page_title') </title>

{{--  Icon in Tab  --}}
<link rel="shortcut icon" href="{{ asset('files/images/logo01.png')}}">

{{--  Google-Fonts CSS  --}}
<link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700,900" rel="stylesheet">
{{--  <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900" rel="stylesheet">  --}}

{{--  Font-Awesome CSS  --}}
<link rel="stylesheet" href="{{ asset('files/css/font-awesome/all.min.css') }}">

{{--  Bootstrap CSS  --}}
<link rel="stylesheet" href="{{ asset('files/css/bootstrap/bootstrap.min.css') }}">

{{-- Vali CSS --}}
<link rel="stylesheet" href="{{ asset('files/css/main.css') }}">

@yield('stylesheet')

{{-- Admin Custom CSS --}}
<link rel="stylesheet" href="{{ asset('files/css/margin-padding.css')}}">
<link rel="stylesheet" href="{{ asset('files/css/style.css')}}">
<link rel="stylesheet" href="{{ asset('files/css/responsive.css')}}">


</head>