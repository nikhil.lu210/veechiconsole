{{-- <!-- Navbar--> --}}
<header class="app-header"><a class="app-header__logo" href="{{ route('home') }}">Veechi</a>
    {{-- <!-- Sidebar toggle button--> --}}
    <a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"><i class="fas fa-bars fa-2x p-t-10"></i></a>

    {{-- <!-- Navbar Right Menu--> --}}
    <ul class="app-nav">
        {{-- <li class="app-search">
            <input class="app-search__input" type="search" placeholder="Search">
            <button class="app-search__button" disabled><i class="fa fa-search"></i></button>
        </li> --}}
        
        {{-- <!-- User Menu--> --}}
        <li class="dropdown">
            <a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu">
                <i class="fa fa-user fa-lg"></i>
                {{-- {{ Auth::user()->name }} --}}
            </a>

            <ul class="dropdown-menu settings-menu dropdown-menu-right">
                {{-- Setting --}}
                <li><a class="dropdown-item" href="{{ route('setting') }}"><i class="fas fa-wrench fa-lg"></i> Setting</a></li>

                {{-- Logout --}}
                <li>
                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <i class="fas fa-power-off p-r-5"></i>Logout
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            </ul>
        </li>
    </ul>
</header>