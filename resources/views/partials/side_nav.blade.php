{{-- <!-- Sidebar menu--> --}}
<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
<aside class="app-sidebar">
    <div class="app-sidebar__user">
        <a href="{{ route('home') }}">
            <img class="app-sidebar__user-avatar" src="{{ asset('files/images/user.jpg') }}" alt="User Image">
        </a>
        <div>
            <p class="app-sidebar__user-name">{{ Auth::user()->name }}</p>
            <p class="app-sidebar__user-designation">Administrator</p>
        </div>
    </div>
    <ul class="app-menu">
        {{-- Dashboard Menu --}}
        <li>
            <a class="app-menu__item" href="{{ route('home') }}">
                <i class="app-menu__icon fas fa-dice-d6"></i>
                <span class="app-menu__label">Dashboard</span>
            </a>
        </li>

        {{-- Records --}}
        <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview">
            <i class="app-menu__icon fas fa-folder"></i>
            <span class="app-menu__label">Records</span><i class="treeview-indicator fa fa-angle-right"></i></a>
            <ul class="treeview-menu">
                <li><a class="treeview-item" href="{{ Route('record.allwebsite.index') }}"><i class="icon far fa-folder"></i> Websites</a></li>
                <li><a class="treeview-item" href="{{ Route('record.allcustomer.index') }}"><i class="icon far fa-folder"></i> Customers</a></li>
            </ul>
        </li>

        {{-- Settings --}}
        <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview">
            <i class="app-menu__icon fas fa-folder"></i>
            <span class="app-menu__label">Settings</span><i class="treeview-indicator fa fa-angle-right"></i></a>
            <ul class="treeview-menu">
                <li><a class="treeview-item" href="{{ Route('setting.website_type.index') }}"><i class="icon far fa-folder"></i> Website Types</a></li>
                <li><a class="treeview-item" href="{{ Route('setting.package.index') }}"><i class="icon far fa-folder"></i> Packages</a></li>
                <li><a class="treeview-item" href="{{ Route('setting.platform.index') }}"><i class="icon far fa-folder"></i> Platforms</a></li>
                <li><a class="treeview-item" href="{{ Route('setting.domain.index') }}"><i class="icon far fa-folder"></i> Domains</a></li>
                <li><a class="treeview-item" href="{{ Route('setting.hosting.index') }}"><i class="icon far fa-folder"></i> Hostings</a></li>
            </ul>
        </li>

    </ul>
</aside>