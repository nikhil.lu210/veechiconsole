@extends('layouts.main')

@section('page_title', '| Hosting Create')

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        label{
            font-weight: 400;
            color: #616161;
        }
        .form-control {
            height: calc(2.25rem + 2px);
            padding: 7px 10px;
            font-weight: 500;
            color: #222222;
            border: 1px solid #dddddd;
            border-radius: .25rem;
            box-shadow: none !important;
            transition: 0.5s all ease-in-out;
        }
        .form-control:focus{
            border-color: #ed1b24;
            transition: 0.5s all ease-in-out;
        }
    </style>
@endsection

@section('content')
    {{--  Body Contents Here  --}}
    
    {{-- ========< Breadcrumb Part Starts >======== --}}
    <section class="breadcrumb-part">
        <div class="app-title">
            <div>
                <h1><i class="far fa-folder-open"></i> <b>Add New Hosting</b></h1>
            </div>
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fas fa-home fa-lg"></i></a></li>
                <li class="breadcrumb-item ">Settings</li>
                <li class="breadcrumb-item"><a href="{{ route('setting.hosting.index') }}">Hostings</a></li>
                <li class="breadcrumb-item active">Add New Hosting</li>
            </ul>
        </div>
    </section>
    {{-- =========< Breadcrumb Part Ends >========= --}}


    {{-- ========< Hosting Part Starts >======== --}}
    <section class="all-domain-part">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 offset-md-3">
                    <div class="card">

                        <div class="card-header">
                            <h3 class="float-left">Add New Hosting</h3>
                            <a href="{{ route('setting.hosting.index') }}" class="btn btn-light btn-sm btn-create m-l-10" data-toggle="tooltip" data-placement="top" title="Back To Previous Page">Back</a>
                        </div>

                        <form action="{{ route('setting.hosting.store') }}" method="post">
                            @csrf
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="hosting" class="control-label">Hosting *</label><br>
                                            <input class="form-control {{ $errors->has('hosting') ? ' has-error' : '' }}" id="hosting" name="hosting" type="text" placeholder="www.website.com">
                                            @if($errors->has('hosting'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('hosting') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="hosting_login_url" class="control-label">Login URL *</label><br>
                                            <input class="form-control {{ $errors->has('hosting_login_url') ? ' has-error' : '' }}" id="hosting_login_url" name="hosting_login_url" type="text" placeholder="www.namespace.com/login">
                                            @if($errors->has('hosting_login_url'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('hosting_login_url') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="hosting_username" class="control-label">Username *</label><br>
                                            <input class="form-control {{ $errors->has('hosting_username') ? ' has-error' : '' }}" id="hosting_username" name="hosting_username" type="text" placeholder="website_user">
                                            @if($errors->has('hosting_username'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('hosting_username') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="hosting_password" class="control-label">Password *</label><br>
                                            <input class="form-control {{ $errors->has('hosting_password') ? ' has-error' : '' }}" id="hosting_password" name="hosting_password" type="text" placeholder="website_user">
                                            @if($errors->has('hosting_password'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('hosting_password') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Note</label>
                                            <textarea class="form-control" rows="3" name="note" placeholder="Short Note"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
    
                            <div class="card-footer">
                                <button class="btn btn-light btn-sm btn-create  m-b-10" type="submit" data-toggle="tooltip" data-placement="top" title="Add New Hosting">Add New Hosting</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{-- =========< Hosting Part Ends >========= --}}
    
@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@endsection