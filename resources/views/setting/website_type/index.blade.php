@extends('layouts.main')

@section('page_title', '| Website Types')

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        .page-item.active .page-link {
            z-index: 1;
            color: #FFF;
            background-color: #ed1b24;
            border-color: #ed1b24;
        }
        .page-link{
            color: #616161;
        }
        .page-link:hover {
            color: #ffffff;
            text-decoration: none;
            background-color: #ed1b24;
            border-color: #ed1b24;
        }
    </style>
@endsection

@section('content')
    {{--  Body Contents Here  --}}
    
    {{-- ========< Breadcrumb Part Starts >======== --}}
    <section class="breadcrumb-part">
        <div class="app-title">
            <div>
                <h1><i class="far fa-folder-open"></i> <b>Website Types</b></h1>
            </div>
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fas fa-home fa-lg"></i></a></li>
                <li class="breadcrumb-item ">Settings</li>
                <li class="breadcrumb-item active">Website Types</li>
            </ul>
        </div>
    </section>
    {{-- =========< Breadcrumb Part Ends >========= --}}


    {{-- ========< Website Type Part Starts >======== --}}
    <section class="website-type-part">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="float-left">All Website Types</h3>
                            <a href="{{ route('setting.website_type.create') }}" class="btn btn-light btn-sm btn-create m-l-10" data-toggle="tooltip" data-placement="top" title="Add New Type">Add New</a>
                        </div>
                        <div class="card-body">
                            <table class="table table-bordered" id="sampleTable">
                                <thead>
                                    <tr>
                                        <th>SL.</th>
                                        <th>Type</th>
                                        <th>Basic Price</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($websiteTypes as $websiteType)
                                    <tr>
                                        <td>{{ $websiteType->id }}</td>
                                        <td>{{ $websiteType->type }}</td>
                                        <td>{{ $websiteType->basic_price }} <sup><b>tk</b></sup></td>
                                        <td style="padding-top: 8px; padding-bottom: 0px;">
                                            <a  onclick="return confirm('Delete! Are you sure?')" href="{{ route('setting.website_type.destroy', ['id' => $websiteType->id]) }}" class="btn btn-light btn-sm btn-delete" data-toggle="tooltip" data-placement="top" title="Delete This Type"><i class="far fa-trash-alt"></i></a>
                                            
                                            <a href="{{ route('setting.website_type.show', ['id' => $websiteType->id]) }}" class="btn btn-light btn-sm btn-view" data-toggle="tooltip" data-placement="top" title="View Type"><i class="far fa-question-circle"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{-- =========< Website Type Part Ends >========= --}}
    
@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script src="{{ asset('files/js/vali/plugins/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('files/js/vali/plugins/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">
        $('#sampleTable').DataTable();

        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@endsection