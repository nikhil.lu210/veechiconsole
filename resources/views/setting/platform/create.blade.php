@extends('layouts.main')

@section('page_title', '| Platform Create')

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        label{
            font-weight: 400;
            color: #616161;
        }
        .form-control {
            height: calc(2.25rem + 2px);
            padding: 7px 10px;
            font-weight: 500;
            color: #222222;
            border: 1px solid #dddddd;
            border-radius: .25rem;
            box-shadow: none !important;
            transition: 0.5s all ease-in-out;
        }
        .form-control:focus{
            border-color: #ed1b24;
            transition: 0.5s all ease-in-out;
        }
    </style>
@endsection

@section('content')
    {{--  Body Contents Here  --}}
    
    {{-- ========< Breadcrumb Part Starts >======== --}}
    <section class="breadcrumb-part">
        <div class="app-title">
            <div>
                <h1><i class="far fa-folder-open"></i> <b>Add New Platform</b></h1>
            </div>
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fas fa-home fa-lg"></i></a></li>
                <li class="breadcrumb-item ">Settings</li>
                <li class="breadcrumb-item"><a href="{{ route('setting.platform.index') }}">Platforms</a></li>
                <li class="breadcrumb-item active">Add New Platform</li>
            </ul>
        </div>
    </section>
    {{-- =========< Breadcrumb Part Ends >========= --}}


    {{-- ========< Platform Part Starts >======== --}}
    <section class="all-platform-part">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 offset-md-3">
                    <div class="card">

                        <div class="card-header">
                            <h3 class="float-left">Add New Platform</h3>
                            <a href="javascript:history.back()" class="btn btn-light btn-sm btn-create m-l-10" data-toggle="tooltip" data-placement="top" title="Back To Previous Page">Back</a>
                        </div>

                        <form action="{{ Route('setting.platform.store') }}" method="post">
                            @csrf
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="platform" class="control-label">Platfrom *</label><br>
                                            <input class="form-control {{ $errors->has('platform') ? ' has-error' : '' }}" id="platform" name="platform" type="text" placeholder="Laravel">
                                            @if($errors->has('platform'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('platform') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="basic_price" class="control-label">Basic Price <sup><b>tk</b></sup></label><br>
                                            <input class="form-control" id="basic_price" name="basic_price" type="text" placeholder="20000">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Note</label>
                                            <textarea class="form-control" rows="3" name="note" placeholder="Short Note"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
    
                            <div class="card-footer">
                                <button class="btn btn-light btn-sm btn-create  m-b-10" type="submit" data-toggle="tooltip" data-placement="top" title="Add New Platform">Add New Platform</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{-- =========< Platform Part Ends >========= --}}
    
@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@endsection