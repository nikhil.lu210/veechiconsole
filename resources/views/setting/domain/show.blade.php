@extends('layouts.main')

@section('page_title', '| View Domain')

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        label{
            font-weight: 400;
            color: #616161;
        }
        .form-control {
            height: calc(2.25rem + 2px);
            padding: 7px 10px;
            font-weight: 500;
            color: #222222;
            border: 1px solid #dddddd;
            border-radius: .25rem;
            box-shadow: none !important;
            transition: 0.5s all ease-in-out;
        }
        .form-control:focus{
            border-color: #ed1b24;
            transition: 0.5s all ease-in-out;
        }
        .form-control:disabled, .form-control[readonly],
        .nice-select.disabled{
            background-color: #ffffff;
            color: #111111;
            border-color: #efefef !important;
        }
    </style>
@endsection

@section('content')
    {{--  Body Contents Here  --}}
    
    {{-- ========< Breadcrumb Part Starts >======== --}}
    <section class="breadcrumb-part">
        <div class="app-title">
            <div>
                <h1><i class="far fa-folder-open"></i> <b>View Domain</b></h1>
            </div>
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fas fa-home fa-lg"></i></a></li>
                <li class="breadcrumb-item ">Settings</li>
                <li class="breadcrumb-item"><a href="{{ route('setting.domain.index') }}">Domains</a></li>
                <li class="breadcrumb-item active">{{$domains->domain}}</li>
            </ul>
        </div>
    </section>
    {{-- =========< Breadcrumb Part Ends >========= --}}


    {{-- ========< Domain Part Starts >======== --}}
    <section class="all-website-type-part">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 offset-md-3">
                    <div class="card">

                        <div class="card-header">
                            <h3 class="float-left">{{$domains->domain}}</h3>
                            <a href="{{ route('setting.domain.index') }}" class="btn btn-light btn-sm btn-create m-l-10" data-toggle="tooltip" data-placement="top" title="Back To Previous Page">Back</a>
                        </div>

                        <form action="{{ route('setting.domain.update', ['id' => $domains->id]) }}" method="post">
                            @csrf
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="domain" class="control-label">Domain *</label><br>
                                            <input class="form-control removeDis" disabled id="domain" name="domain" type="text" value="{{$domains->domain}}">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="domain_login_url" class="control-label">Login URL *</label><br>
                                            <input class="form-control removeDis" disabled id="domain_login_url" name="domain_login_url" type="text" value="{{$domains->domain_login_url}}">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="domain_username" class="control-label">Username *</label><br>
                                            <input class="form-control removeDis" disabled id="domain_username" name="domain_username" type="text" value="{{$domains->domain_username}}">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="domain_password" class="control-label">Password *</label><br>
                                            <input class="form-control removeDis" disabled id="domain_password" name="domain_password" type="text" value="{{$domains->domain_password}}">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Note</label>
                                            <textarea class="form-control removeDis" disabled rows="3" name="note">{{$domains->note}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
    
                            <div class="card-footer">
                                <div class="float-right m-b-10" role="group" aria-label="Basic example">
                                    <button type="button" class="btn btn-light btn-create btn-sm" id="removeDisabled">Edit</button>
    
                                    <button type="submit" class="btn btn-light btn-create btn-sm hiddenButton d-none">Update</button>
    
                                    <button type="button" class="btn btn-light btn-create btn-sm hiddenButton d-none" id="addDisabled" style="margin-right: 5px;">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{-- =========< Domain Part Ends >========= --}}
    
@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@endsection