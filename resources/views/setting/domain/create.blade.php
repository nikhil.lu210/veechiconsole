@extends('layouts.main')

@section('page_title', '| Domain Create')

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        label{
            font-weight: 400;
            color: #616161;
        }
        .form-control {
            height: calc(2.25rem + 2px);
            padding: 7px 10px;
            font-weight: 500;
            color: #222222;
            border: 1px solid #dddddd;
            border-radius: .25rem;
            box-shadow: none !important;
            transition: 0.5s all ease-in-out;
        }
        .form-control:focus{
            border-color: #ed1b24;
            transition: 0.5s all ease-in-out;
        }
    </style>
@endsection

@section('content')
    {{--  Body Contents Here  --}}
    
    {{-- ========< Breadcrumb Part Starts >======== --}}
    <section class="breadcrumb-part">
        <div class="app-title">
            <div>
                <h1><i class="far fa-folder-open"></i> <b>Add New Domain</b></h1>
            </div>
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fas fa-home fa-lg"></i></a></li>
                <li class="breadcrumb-item ">Settings</li>
                <li class="breadcrumb-item"><a href="{{ route('setting.domain.index') }}">Domains</a></li>
                <li class="breadcrumb-item active">Add New Domain</li>
            </ul>
        </div>
    </section>
    {{-- =========< Breadcrumb Part Ends >========= --}}


    {{-- ========< Domain Part Starts >======== --}}
    <section class="all-domain-part">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 offset-md-3">
                    <div class="card">

                        <div class="card-header">
                            <h3 class="float-left">Add New Domain</h3>
                            <a href="{{ route('setting.domain.index') }}" class="btn btn-light btn-sm btn-create m-l-10" data-toggle="tooltip" data-placement="top" title="Back To Previous Page">Back</a>
                        </div>

                        <form action="{{ route('setting.domain.store') }}" method="post">
                            @csrf
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="domain" class="control-label">Domain *</label><br>
                                            <input class="form-control {{ $errors->has('domain') ? ' has-error' : '' }}" id="domain" name="domain" type="text" placeholder="www.website.com">
                                            @if($errors->has('domain'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('domain') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="domain_login_url" class="control-label">Login URL *</label><br>
                                            <input class="form-control {{ $errors->has('domain_login_url') ? ' has-error' : '' }}" id="domain_login_url" name="domain_login_url" type="text" placeholder="www.namespace.com/login">
                                            @if($errors->has('domain_login_url'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('domain_login_url') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="domain_username" class="control-label">Username *</label><br>
                                            <input class="form-control {{ $errors->has('domain_username') ? ' has-error' : '' }}" id="domain_username" name="domain_username" type="text" placeholder="website_user">
                                            @if($errors->has('domain_username'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('domain_username') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="domain_password" class="control-label">Password *</label><br>
                                            <input class="form-control {{ $errors->has('domain_password') ? ' has-error' : '' }}" id="domain_password" name="domain_password" type="text" placeholder="website_user">
                                            @if($errors->has('domain_password'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('domain_password') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Note</label>
                                            <textarea class="form-control" rows="3" name="note" placeholder="Short Note"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
    
                            <div class="card-footer">
                                <button class="btn btn-light btn-sm btn-create  m-b-10" type="submit" data-toggle="tooltip" data-placement="top" title="Add New Package">Add New Domain</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{-- =========< Domain Part Ends >========= --}}
    
@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@endsection