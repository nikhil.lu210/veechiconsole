@extends('layouts.main')

@section('page_title', '| Domain')

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        .page-item.active .page-link {
            z-index: 1;
            color: #FFF;
            background-color: #ed1b24;
            border-color: #ed1b24;
        }
        .page-link{
            color: #616161;
        }
        .page-link:hover {
            color: #ffffff;
            text-decoration: none;
            background-color: #ed1b24;
            border-color: #ed1b24;
        }

        button.btn-copy,
        button.btn-copy:hover,
        button.btn-copy:focus,
        button.btn-copy:active{
            background: #fff !important;
            border: 0px solid;
            color: #ed1b24 !important;
            box-shadow: none !important;
            transform: none !important;
            outline: none !important;
        }
    </style>
@endsection

@section('content')
    {{--  Body Contents Here  --}}
    
    {{-- ========< Breadcrumb Part Starts >======== --}}
    <section class="breadcrumb-part">
        <div class="app-title">
            <div>
                <h1><i class="far fa-folder-open"></i> <b>Domains</b></h1>
            </div>
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fas fa-home fa-lg"></i></a></li>
                <li class="breadcrumb-item ">Settings</li>
                <li class="breadcrumb-item active">Domains</li>
            </ul>
        </div>
    </section>
    {{-- =========< Breadcrumb Part Ends >========= --}}


    {{-- ========< Domain Part Starts >======== --}}
    <section class="domain-part">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="float-left">All Domains</h3>
                            <a href="{{ route('setting.domain.create') }}" class="btn btn-light btn-sm btn-create m-l-10" data-toggle="tooltip" data-placement="top" title="Add New Domain">Add New</a>
                        </div>
                        <div class="card-body">
                            <table class="table table-bordered" id="sampleTable">
                                <thead>
                                    <tr>
                                        <th>SL.</th>
                                        <th>Domain</th>
                                        <th>Login URL</th>
                                        <th>Username</th>
                                        <th>Password</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($domains as $domain)
                                    <tr>
                                        <td>{{$domain->id}}</td>
                                        <td>{{$domain->domain}}</td>
                                        <td>
                                            <button class="btn btn-info btn-sm btn-copy" data-clipboard-text="{{$domain->domain_login_url}}">
                                                Copy URL
                                            </button>
                                        </td>
                                        <td>
                                            <button class="btn btn-info btn-sm btn-copy" data-clipboard-text="{{$domain->domain_username}}">
                                                Copy Username
                                            </button>
                                        </td>
                                        <td>
                                            <button class="btn btn-info btn-sm btn-copy" data-clipboard-text="{{$domain->domain_password}}">
                                                    Copy Password
                                            </button>
                                        </td>
                                        <td style="padding-top: 8px; padding-bottom: 0px;">
                                            <a onclick="return confirm('Delete! Are you sure?')" href="{{ route('setting.domain.destroy', ['id' => $domain->id]) }}" class="btn btn-light btn-sm btn-delete" data-toggle="tooltip" data-placement="top" title="Delete This Record"><i class="far fa-trash-alt"></i></a>
                                            
                                            <a href="{{ route('setting.domain.show', ['id' => $domain->id]) }}" class="btn btn-light btn-sm btn-view" data-toggle="tooltip" data-placement="top" title="View Domain Details"><i class="far fa-question-circle"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{-- =========< Domain Part Ends >========= --}}
    
@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script src="{{ asset('files/js/vali/plugins/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('files/js/vali/plugins/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{ asset('files/js/clipboard/clipboard.min.js')}}"></script>
    <script type="text/javascript">
        $('#sampleTable').DataTable();

        new ClipboardJS('.btn');

        $(".btn-copy").click(function () {
            $(this).slideUp( 20 ).fadeIn( 700 ).text("Copied...");
        });

        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@endsection