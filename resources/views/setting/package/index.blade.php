@extends('layouts.main')

@section('page_title', '| Package')

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        .page-item.active .page-link {
            z-index: 1;
            color: #FFF;
            background-color: #ed1b24;
            border-color: #ed1b24;
        }
        .page-link{
            color: #616161;
        }
        .page-link:hover {
            color: #ffffff;
            text-decoration: none;
            background-color: #ed1b24;
            border-color: #ed1b24;
        }
    </style>
@endsection

@section('content')
    {{--  Body Contents Here  --}}
    
    {{-- ========< Breadcrumb Part Starts >======== --}}
    <section class="breadcrumb-part">
        <div class="app-title">
            <div>
                <h1><i class="far fa-folder-open"></i> <b>Packages</b></h1>
            </div>
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fas fa-home fa-lg"></i></a></li>
                <li class="breadcrumb-item ">Settings</li>
                <li class="breadcrumb-item active">Packages</li>
            </ul>
        </div>
    </section>
    {{-- =========< Breadcrumb Part Ends >========= --}}


    {{-- ========< Package Part Starts >======== --}}
    <section class="package-part">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="float-left">All Packages</h3>
                            <a href="{{ route('setting.package.create') }}" class="btn btn-light btn-sm btn-create m-l-10" data-toggle="tooltip" data-placement="top" title="Add New Package">Add New</a>
                        </div>
                        <div class="card-body">
                            <table class="table table-bordered" id="sampleTable">
                                <thead>
                                    <tr>
                                        <th>SL.</th>
                                        <th>Package</th>
                                        <th>Basic Price</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($packages as $package)
                                    <tr>
                                        <td>{{$package->id}}</td>
                                        <td>{{$package->package}}</td>
                                        <td>{{$package->basic_price}} <sup><b>tk</b></sup></td>
                                        <td style="padding-top: 8px; padding-bottom: 0px;">
                                            <a onclick="return confirm('Delete! Are you sure?')" href="{{ route('setting.package.destroy', ['id' => $package->id]) }}" class="btn btn-light btn-sm btn-delete" data-toggle="tooltip" data-placement="top" title="Delete This Type"><i class="far fa-trash-alt"></i></a>
                                            
                                            <a href="{{ route('setting.package.show', ['id' => $package->id]) }}" class="btn btn-light btn-sm btn-view" data-toggle="tooltip" data-placement="top" title="View Type"><i class="far fa-question-circle"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{-- =========< Package Part Ends >========= --}}
    
@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script src="{{ asset('files/js/vali/plugins/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('files/js/vali/plugins/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">
        $('#sampleTable').DataTable();

        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@endsection