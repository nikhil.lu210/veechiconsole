@extends('layouts.main')

@section('page_title', '| View Package')

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        label{
            font-weight: 400;
            color: #616161;
        }
        .form-control {
            height: calc(2.25rem + 2px);
            padding: 7px 10px;
            font-weight: 500;
            color: #222222;
            border: 1px solid #dddddd;
            border-radius: .25rem;
            box-shadow: none !important;
            transition: 0.5s all ease-in-out;
        }
        .form-control:focus{
            border-color: #ed1b24;
            transition: 0.5s all ease-in-out;
        }
        .form-control:disabled, .form-control[readonly],
        .nice-select.disabled{
            background-color: #ffffff;
            color: #111111;
            border-color: #efefef !important;
        }
    </style>
@endsection

@section('content')
    {{--  Body Contents Here  --}}
    
    {{-- ========< Breadcrumb Part Starts >======== --}}
    <section class="breadcrumb-part">
        <div class="app-title">
            <div>
                <h1><i class="far fa-folder-open"></i> <b>View Package</b></h1>
            </div>
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fas fa-home fa-lg"></i></a></li>
                <li class="breadcrumb-item ">Settings</li>
                <li class="breadcrumb-item"><a href="{{ route('setting.package.index') }}">Packages</a></li>
                <li class="breadcrumb-item active">{{$packages->package}}</li>
            </ul>
        </div>
    </section>
    {{-- =========< Breadcrumb Part Ends >========= --}}


    {{-- ========< Package Part Starts >======== --}}
    <section class="all-website-type-part">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 offset-md-3">
                    <div class="card">

                        <div class="card-header">
                            <h3 class="float-left">{{$packages->package}}</h3>
                            <a href="{{ route('setting.package.index') }}" class="btn btn-light btn-sm btn-create m-l-10" data-toggle="tooltip" data-placement="top" title="Back To Previous Page">Back</a>
                        </div>

                        <form action="{{ route('setting.package.update', ['id' => $packages->id]) }}" method="post">
                            @csrf
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="package" class="control-label">Package *</label><br>
                                            <input class="form-control removeDis" disabled id="package" name="package" type="text" value="{{$packages->package}}">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="basic_price" class="control-label">Basic Price <sup><b>tk</b></sup></label><br>
                                            <input class="form-control removeDis" disabled id="basic_price" name="basic_price" type="text" value="{{$packages->basic_price}}">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Note</label>
                                            <textarea class="form-control removeDis" disabled rows="3" name="note">{{$packages->note}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
    
                            <div class="card-footer">
                                <div class="float-right m-b-10" role="group" aria-label="Basic example">
                                    <button type="button" class="btn btn-light btn-create btn-sm" id="removeDisabled">Edit</button>
    
                                    <button type="submit" class="btn btn-light btn-create btn-sm hiddenButton d-none">Update</button>
    
                                    <button type="button" class="btn btn-light btn-create btn-sm hiddenButton d-none" id="addDisabled" style="margin-right: 5px;">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{-- =========< Package Part Ends >========= --}}
    
@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@endsection