@extends('layouts.main')

@section('page_title', '| Website')

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        .page-item.active .page-link {
            z-index: 1;
            color: #FFF;
            background-color: #ed1b24;
            border-color: #ed1b24;
        }
        .page-link{
            color: #616161;
        }
        .page-link:hover {
            color: #ffffff;
            text-decoration: none;
            background-color: #ed1b24;
            border-color: #ed1b24;
        }
        .sweet-alert button{
            background-color: #ed1b24 !important;
        }
        .sweet-alert button.cancel{
            background-color: #6abb00 !important;
        }

        button.btn-copy,
        button.btn-copy:hover,
        button.btn-copy:focus,
        button.btn-copy:active{
            background: #fff !important;
            border: 0px solid;
            color: #ed1b24 !important;
            box-shadow: none !important;
            transform: none !important;
            outline: none !important;
        }
        .status{
            color: #ed1b24;
            font-size: 16px;
        }
        .status.active{
            color: #00cc00;
        }
    </style>
@endsection

@section('content')
    {{--  Body Contents Here  --}}
    
    {{-- ========< Breadcrumb Part Starts >======== --}}
    <section class="breadcrumb-part">
        <div class="app-title">
            <div>
                <h1><i class="far fa-folder-open"></i> <b>All Websites</b></h1>
            </div>
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fas fa-home fa-lg"></i></a></li>
                <li class="breadcrumb-item ">Records</li>
                <li class="breadcrumb-item active">All Websites</li>
            </ul>
        </div>
    </section>
    {{-- =========< Breadcrumb Part Ends >========= --}}


    {{-- ========< All Website Part Starts >======== --}}
    <section class="all-website-part">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="float-left">All Websites</h3>
                            <a href="{{ route('record.allwebsite.create') }}" class="btn btn-light btn-sm btn-create m-l-10" data-toggle="tooltip" data-placement="top" title="Add New Website Record">Add New</a>
                        </div>
                        <div class="card-body">
                            <table class="table table-bordered" id="sampleTable">
                                <thead>
                                    <tr>
                                        <th>Website Name</th>
                                        <th>Login URL</th>
                                        <th>Username</th>
                                        <th>Password</th>
                                        <th class="text-center">Status</th>
                                        <th>Dev. Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($allwebsites as $website)
                                    <tr>
                                        <td>{{$website->name}}</td>
                                        <td>
                                            <button class="btn btn-info btn-sm btn-copy" data-clipboard-text="{{$website->website_login_url}}">
                                                Copy URL
                                            </button>
                                        </td>
                                        <td>
                                            <button class="btn btn-info btn-sm btn-copy" data-clipboard-text="{{$website->website_username}}">
                                                Copy Username
                                            </button>
                                        </td>
                                        <td>
                                            <button class="btn btn-info btn-sm btn-copy" data-clipboard-text="{{$website->website_password}}">
                                                Copy Password
                                            </button>
                                        </td>
                                        <td class="text-center">
                                            <?php
                                                $curDate = strtotime(date('d-m-Y'));
                                                $expDate = strtotime(date($website->expire_date));
                                            ?>
                                            @if ($curDate < $expDate)
                                                <i class="fas fa-lightbulb status active"></i>
                                            @else
                                                <i class="fas fa-lightbulb status"></i>
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            @if ($website->dev_status == 1)
                                                <i class="fas fa-sync-alt status"></i>
                                            @else
                                                <i class="far fa-check-circle status active"></i>
                                            @endif
                                        </td>
                                        <td style="padding-top: 8px; padding-bottom: 0px;">
                                            <a onclick="return confirm('Delete! Are you sure?')" href="{{ route('record.allwebsite.destroy', ['id' => $website->id]) }}" class="btn btn-light btn-sm btn-delete" data-toggle="tooltip" data-placement="top" title="Delete This Record"><i class="far fa-trash-alt"></i></a>
                                            
                                            <a href="{{ route('record.allwebsite.show', ['id' => $website->id]) }}" class="btn btn-light btn-sm btn-view" data-toggle="tooltip" data-placement="top" title="View Record"><i class="far fa-question-circle"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{-- =========< All Website Part Ends >========= --}}
    
@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script src="{{ asset('files/js/vali/plugins/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('files/js/vali/plugins/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{ asset('files/js/clipboard/clipboard.min.js')}}"></script>
    <script type="text/javascript">
        $('#sampleTable').DataTable();

        new ClipboardJS('.btn');

        $(".btn-copy").click(function () {
            $(this).slideUp( 20 ).fadeIn( 700 ).text("Copied...");
        });

        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@endsection