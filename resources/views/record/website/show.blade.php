@extends('layouts.main')

@section('page_title', '| Show Website')

@section('stylesheet')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('files/css/nice-select/nice-select.css') }}">
    <link rel="stylesheet" href="{{ asset('files/css/datepicker/bootstrapDatepickr-1.0.0.min.css') }}">
    <style>
        .page-item.active .page-link {
            z-index: 1;
            color: #FFF;
            background-color: #ed1b24;
            border-color: #ed1b24;
        }
        .page-link{
            color: #616161;
        }
        .page-link:hover {
            color: #ffffff;
            text-decoration: none;
            background-color: #ed1b24;
            border-color: #ed1b24;
        }
        .sweet-alert button{
            background-color: #ed1b24 !important;
        }
        .sweet-alert button.cancel{
            background-color: #6abb00 !important;
        }

        button.btn-copy,
        button.btn-copy:hover,
        button.btn-copy:focus,
        button.btn-copy:active{
            background: #fff !important;
            border: 0px solid;
            color: #ed1b24 !important;
            box-shadow: none !important;
            transform: none !important;
            outline: none !important;
        }

        label{
            font-weight: 400;
            color: #616161;
        }
        .form-control {
            height: calc(2.25rem + 2px);
            padding: 7px 10px;
            font-weight: 500;
            color: #222222;
            border: 1px solid #dddddd;
            border-radius: .25rem;
            box-shadow: none !important;
            transition: 0.5s all ease-in-out;
        }
        .form-control:focus{
            border-color: #ed1b24;
            transition: 0.5s all ease-in-out;
        }
        .nice-select.small {
            margin-bottom: 20px;
            font-weight: 500;
            font-size: 14px;
            height: calc(2.25rem + 2px);
            line-height: 38px;
            border-color: #dddddd;
            transition: 0.5s all ease-in-out;
        }
        .nice-select:active,
        .nice-select.open,
        .nice-select:focus{
            border-color: #ced2d6;
            transition: 0.5s all ease-in-out;
        }
        .nice-select.small .option {
            font-weight: 500;
        }

        .form-control:disabled, .form-control[readonly],
        .nice-select.disabled{
            background-color: #ffffff;
            color: #111111;
            border-color: #efefef !important;
        }
        .select2-container .select2-selection--single{
            height: calc(2.25rem + 2px);
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered {
            line-height: 35px;
            font-weight: 500;
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered{
            line-height: 35px;
        }
        .select2-container--default .select2-selection--single .select2-selection__arrow{
            top: 5px;
        }
        .select2-container--default .select2-selection--single{
            border-color: #ced4da;
            position: relative;
        }
        .select2-container--default .select2-search--dropdown .select2-search__field {
            border: 1px solid #ced4da;
        }
        .select2-container--default .select2-search--dropdown .select2-search__field:focus{
            outline: none !important;
            box-shadow: none !important;
            border-color: #ced4da;
        }
        .select2-container--default.select2-container--open.select2-container--below .select2-selection--single, .select2-container--default.select2-container--open.select2-container--below .select2-selection--multiple{
            border-color: #ed1b24;
        }
        .select2-container--default .select2-results__option{
            color: #000000;
            font-weight: 500;
        }
        .select2-container--default .select2-results__option--highlighted[aria-selected] {
            background-color: #f6f6f6;
            color: #000000;
            font-weight: 500;
        }
        span.select2.select2-container.select2-container--default,
        span.select2.select2-container.select2-container--default.select2-container--above.select2-container--focus,
        span.select2.select2-container.select2-container--default.select2-container--above.select2-container--open{
            width: 100% !important;
        }

        .select2-container--default.select2-container--open.select2-container--above .select2-selection--single, .select2-container--default.select2-container--open.select2-container--above .select2-selection--multiple {
            border-top-left-radius: 0;
            border-top-right-radius: 0;
            border-color: #ed1b24;
        }
        .select2-dropdown {
            background-color: white;
            border: 1px solid #aaa;
        }
        .select2-container--default .select2-selection--single .select2-selection__arrow b{
            top: 30%;
        }
        .select2-container--default.select2-container--disabled .select2-selection--single{
            background-color: #ffffff;
            border-color: #efefef;
        }
        .select2-container--default .select2-selection--single{
            border-color: #ced4da;
            border-width: 1px;
        }
    </style>
@endsection

@section('content')
    {{--  Body Contents Here  --}}

    {{-- ========< Breadcrumb Part Starts >======== --}}
    <section class="breadcrumb-part">
        <div class="app-title">
            <div>
                <h1><i class="far fa-folder-open"></i> <b>Website Details</b></h1>
            </div>
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fas fa-home fa-lg"></i></a></li>
                <li class="breadcrumb-item ">Records</li>
                <li class="breadcrumb-item"><a href="{{ route('record.allwebsite.index') }}">All Websites</a></li>
                <li class="breadcrumb-item active">{{$allwebsites->name}}</li>
            </ul>
        </div>
    </section>
    {{-- =========< Breadcrumb Part Ends >========= --}}


    {{-- ========< All Website Part Starts >======== --}}
    <section class="all-website-part">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">

                        <div class="card-header">
                            <h3 class="float-left">{{$allwebsites->name}}</h3>
                            <a href="{{ route('record.allwebsite.index') }}" class="btn btn-light btn-sm btn-create m-l-10" data-toggle="tooltip" data-placement="top" title="Back To Previous Page">Back</a>
                        </div>

                        <form action="{{ route('record.allwebsite.update', ['id' => $allwebsites->id]) }}" method="POST">
                            @csrf
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Website Name *</label>
                                            <input class="form-control removeDis" disabled name="name" type="text" value="{{$allwebsites->name}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Domain</label><br>
                                            <select class="small wide select-two removeDis" disabled name="domain">
                                                <option value="{{$allwebsites->domain}}">{{$allwebsites->domain}}</option>
                                                @foreach ($domains as $domain)
                                                    <option value="{{ $domain->domain }}">{{ $domain->domain }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Hosting</label><br>
                                            <select class="small wide select-two removeDis" disabled name="hosting">
                                                <option value="{{$allwebsites->hosting}}">{{$allwebsites->hosting}}</option>
                                                @foreach ($hostings as $hosting)
                                                    <option value="{{ $hosting->hosting }}">{{ $hosting->hosting }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Customer Name</label><br>
                                            <select class="small wide select-two removeDis" disabled name="customer_name">
                                                <option value="{{$allwebsites->customer_name}}">{{$allwebsites->customer_name}}</option>
                                                @foreach ($customers as $customer)
                                                    <option value="{{ $customer->customer_name }}">{{ $customer->customer_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">Website Type</label><br>
                                            <select class="small wide nice-select removeDis" disabled name="type">
                                                <option value="{{$allwebsites->type}}">{{$allwebsites->type}}</option>
                                                @foreach ($websiteTypes as $type)
                                                    <option value="{{ $type->type }}">{{ $type->type }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">Package</label><br>
                                            <select class="small wide nice-select removeDis" disabled name="package">
                                                <option value="{{$allwebsites->package}}">{{$allwebsites->package}}</option>
                                                @foreach ($packages as $package)
                                                    <option value="{{ $package->package }}">{{ $package->package }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">Platform</label><br>
                                            <select class="small wide nice-select removeDis" disabled name="platform">
                                                <option value="{{$allwebsites->platform}}">{{$allwebsites->platform}}</option>
                                                @foreach ($platforms as $platform)
                                                    <option value="{{ $platform->platform }}">{{ $platform->platform }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">Development Status</label><br>
                                            <select class="small wide nice-select removeDis" disabled name="dev_status">

                                                @if ($allwebsites->dev_status == 1)
                                                    <option value="1">Under Development</option>
                                                @else
                                                    <option value="2">Developed</option>
                                                @endif

                                                <option value="1">Under Development</option>
                                                <option value="2">Developed</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Registration Date</label>
                                            <input class="form-control removeDis" disabled id="register_date" name="register_date" type="text" value="{{$allwebsites->register_date}}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Registration Expire Date</label>
                                            <input class="form-control removeDis" disabled id="expire_date" name="expire_date" type="text" value="{{$allwebsites->expire_date}}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Domain Expired Date</label>
                                            <input id="domain_expire_date" name="domain_expire_date" class="form-control removeDis" disabled type="text" value="{{$allwebsites->domain_expire_date}}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Website Login URL</label>
                                            <input class="form-control removeDis" disabled name="website_login_url" type="text" value="{{$allwebsites->website_login_url}}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Website Username</label>
                                            <input class="form-control removeDis" disabled name="website_username" type="text" value="{{$allwebsites->website_username}}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Website Password</label>
                                            <input class="form-control removeDis" disabled name="website_password" type="text" value="{{$allwebsites->website_password}}">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Note</label>
                                            <textarea class="form-control removeDis" disabled rows="3" name="note">{{$allwebsites->note}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="card-footer">
                                <div class="float-right m-b-10" role="group" aria-label="Basic example">
                                    <button type="button" class="btn btn-light btn-create btn-sm" id="removeDisabled">Edit</button>

                                    <button type="submit" class="btn btn-light btn-create btn-sm hiddenButton d-none">Update</button>

                                    <button type="button" class="btn btn-light btn-create btn-sm hiddenButton d-none" id="addDisabled" style="margin-right: 5px;">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{-- =========< All Website Part Ends >========= --}}

@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script src="{{ asset('files/js/nice-select/jquery.nice-select.min.js') }}"></script>
    <script src="{{ asset('files/js/datepicker/bootstrapDatepickr-1.0.0.min.js') }}"></script>
    <script src="{{ asset('files/js/select2/select2.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#register_date, #expire_date, #domain_expire_date").bootstrapDatepickr({
                date_format: "d-m-Y"
            });

            $('.nice-select').niceSelect();

            $('.select-two').select2();
        });
    </script>
@endsection
