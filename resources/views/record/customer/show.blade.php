@extends('layouts.main')

@section('page_title', '| View Customer')

@section('stylesheet')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('files/css/nice-select/nice-select.css') }}">
    <link rel="stylesheet" href="{{ asset('files/css/datepicker/bootstrapDatepickr-1.0.0.min.css') }}">
    <style>
        .page-item.active .page-link {
            z-index: 1;
            color: #FFF;
            background-color: #ed1b24;
            border-color: #ed1b24;
        }
        .page-link{
            color: #616161;
        }
        .page-link:hover {
            color: #ffffff;
            text-decoration: none;
            background-color: #ed1b24;
            border-color: #ed1b24;
        }

        label{
            font-weight: 400;
            color: #616161;
        }
        .form-control {
            height: calc(2.25rem + 2px);
            padding: 7px 10px;
            font-weight: 500;
            color: #222222;
            border: 1px solid #dddddd;
            border-radius: .25rem;
            box-shadow: none !important;
            transition: 0.5s all ease-in-out;
        }
        .form-control:focus{
            border-color: #ed1b24;
            transition: 0.5s all ease-in-out;
        }
        .form-control:disabled, .form-control[readonly],
        .nice-select.disabled{
            background-color: #ffffff;
            color: #111111;
            border-color: #efefef !important;
        }
        .nice-select.small {
            margin-bottom: 20px;
            font-weight: 500;
            font-size: 14px;
            height: calc(2.25rem + 2px);
            line-height: 38px;
            border-color: #dddddd;
            transition: 0.5s all ease-in-out;
        }
        .nice-select:active, 
        .nice-select.open, 
        .nice-select:focus{
            border-color: #ced2d6;
            transition: 0.5s all ease-in-out;
        }
        .nice-select.small .option {
            font-weight: 500;
        }
    </style>
@endsection

@section('content')
    {{--  Body Contents Here  --}}
    
    {{-- ========< Breadcrumb Part Starts >======== --}}
    <section class="breadcrumb-part">
        <div class="app-title">
            <div>
                <h1><i class="far fa-folder-open"></i> <b>Customer Details</b></h1>
            </div>
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fas fa-home fa-lg"></i></a></li>
                <li class="breadcrumb-item ">Records</li>
                <li class="breadcrumb-item"><a href="{{ route('record.allcustomer.index') }}">All Customers</a></li>
                <li class="breadcrumb-item active">{{$allcustomers->customer_name}}</li>
            </ul>
        </div>
    </section>
    {{-- =========< Breadcrumb Part Ends >========= --}}


    {{-- ========< All Customer Part Starts >======== --}}
    <section class="all-customer-part">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">

                        <div class="card-header">
                            <h3 class="float-left">{{$allcustomers->customer_name}}</h3>
                            <a href="{{ route('record.allcustomer.index') }}" class="btn btn-light btn-sm btn-create m-l-10" data-toggle="tooltip" data-placement="top" title="Back To Previous Page">Back</a>
                        </div>

                        <form action="{{ route('record.allcustomer.update', ['id' => $allcustomers->id]) }}" method="POST">
                            @csrf
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="customer_name" class="control-label {{ $errors->has('customer_name') ? ' has-error' : '' }}">Customer Name *</label><br>
                                            <input class="form-control removeDis" disabled id="customer_name" name="customer_name" type="text" value="{{$allcustomers->customer_name}}">
                                            @if($errors->has('customer_name'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('customer_name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Joining Date *</label>
                                            <input class="form-control removeDis {{ $errors->has('joining_date') ? ' has-error' : '' }}" disabled id="joining_date" name="joining_date" type="text" value="{{$allcustomers->joining_date}}">
                                            @if($errors->has('joining_date'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('joining_date') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="mobile_number" class="control-label">Mobile No *</label><br>
                                            <input class="form-control removeDis {{ $errors->has('mobile_number') ? ' has-error' : '' }}" disabled id="mobile_number" name="mobile_number" type="text" value="{{$allcustomers->mobile_number}}">
                                            @if($errors->has('mobile_number'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('mobile_number') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="email" class="control-label">Email *</label><br>
                                            <input class="form-control removeDis {{ $errors->has('email') ? ' has-error' : '' }}" disabled id="email" name="email" type="text" value="{{$allcustomers->email}}">
                                            @if($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Status *</label><br>
                                            <select class="small wide nice-select removeDis {{ $errors->has('status') ? ' has-error' : '' }}" disabled name="status">
                                                <option data-display="{{$allcustomers->status}}">{{$allcustomers->status}}</option>
                                                <option value="active">Active</option>
                                                <option value="inactive">Inactive</option>
                                            </select>
                                            @if($errors->has('status'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('status') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Address</label>
                                            <textarea class="form-control removeDis" disabled rows="3" name="address">{{$allcustomers->address}}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Note</label>
                                            <textarea class="form-control removeDis" disabled rows="3" name="note">{{$allcustomers->note}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
    
                            <div class="card-footer">
                                <div class="float-right m-b-10" role="group" aria-label="Basic example">
                                    <button type="button" class="btn btn-light btn-create btn-sm" id="removeDisabled">Edit</button>
    
                                    <button type="submit" class="btn btn-light btn-create btn-sm hiddenButton d-none">Update</button>
    
                                    <button type="button" class="btn btn-light btn-create btn-sm hiddenButton d-none" id="addDisabled" style="margin-right: 5px;">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{-- =========< All Website Part Ends >========= --}}
    
@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script src="{{ asset('files/js/nice-select/jquery.nice-select.min.js') }}"></script>
    <script src="{{ asset('files/js/datepicker/bootstrapDatepickr-1.0.0.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#joining_date").bootstrapDatepickr({
                date_format: "d-m-Y"
            });
            
            $('.nice-select').niceSelect();
        });
    </script>
@endsection