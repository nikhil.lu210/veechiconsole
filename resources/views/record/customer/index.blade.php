@extends('layouts.main')

@section('page_title', '| Customer')

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        .page-item.active .page-link {
            z-index: 1;
            color: #FFF;
            background-color: #ed1b24;
            border-color: #ed1b24;
        }
        .page-link{
            color: #616161;
        }
        .page-link:hover {
            color: #ffffff;
            text-decoration: none;
            background-color: #ed1b24;
            border-color: #ed1b24;
        }
        .status{
            color: #ed1b24;
            font-size: 16px;
        }
        .status.active{
            color: #00cc00;
        }
    </style>
@endsection

@section('content')
    {{--  Body Contents Here  --}}
    
    {{-- ========< Breadcrumb Part Starts >======== --}}
    <section class="breadcrumb-part">
        <div class="app-title">
            <div>
                <h1><i class="far fa-folder-open"></i> <b>All Customers</b></h1>
            </div>
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fas fa-home fa-lg"></i></a></li>
                <li class="breadcrumb-item ">Records</li>
                <li class="breadcrumb-item active">All Customers</li>
            </ul>
        </div>
    </section>
    {{-- =========< Breadcrumb Part Ends >========= --}}


    {{-- ========< All Customers Part Starts >======== --}}
    <section class="all-customer-part">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="float-left">All Customers</h3>
                            <a href="{{ route('record.allcustomer.create') }}" class="btn btn-light btn-sm btn-create m-l-10" data-toggle="tooltip" data-placement="top" title="Add New Customer">Add New</a>
                        </div>
                        <div class="card-body">
                            <table class="table table-bordered" id="sampleTable">
                                <thead>
                                    <tr>
                                        <th>Customer Name</th>
                                        <th>Joining Date</th>
                                        <th>Number</th>
                                        <th>Email</th>
                                        <th class="text-center">Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($allcustomers as $customer)
                                    <tr>
                                        <td>{{$customer->customer_name}}</td>
                                        <td>{{$customer->joining_date}}</td>
                                        <td>{{$customer->mobile_number}}</td>
                                        <td>{{$customer->email}}</td>
                                        <td class="text-center">
                                            <i class="fas fa-lightbulb status {{$customer->status}}"></i> 
                                            <span class="d-none">{{$customer->status}}</span>
                                        </td>
                                        <td style="padding-top: 8px; padding-bottom: 0px;">
                                            <a onclick="return confirm('Delete! Are you sure?')" href="{{ route('record.allcustomer.destroy', ['id' => $customer->id]) }}" class="btn btn-light btn-sm btn-delete" data-toggle="tooltip" data-placement="top" title="Delete This Record"><i class="far fa-trash-alt"></i></a>
                                            
                                            <a href="{{ route('record.allcustomer.show', ['id' => $customer->id]) }}" class="btn btn-light btn-sm btn-view" data-toggle="tooltip" data-placement="top" title="View Record"><i class="far fa-question-circle"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{-- =========< All Customers Part Ends >========= --}}
    
@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script src="{{ asset('files/js/vali/plugins/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('files/js/vali/plugins/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">
        $('#sampleTable').DataTable();

        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@endsection