<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('partials.header') 
    {{--  This will include all CSS files which are connected into header.blade.php in partials Folder  --}}
</head>
<body class="app sidebar-mini rtl">
    <div id="app">

         @include('partials.top_nav') 
         @include('partials.side_nav') 

        <main class="app-content">
            @yield('content')
        </main>
        @include('partials.footer') 
        {{--  This will include The Footer Design which are connected into footer.blade.php in partials Folder  --}}
        
    </div>

    @include('partials.javascript')
    {{--  This will include all JS files which are connected into javascript.blade.php in partials Folder  --}}
    

</body>
</html>