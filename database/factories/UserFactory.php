<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Model\Record\AllWebsite::class, function (Faker $faker) {

    $type = $faker->randomElement(['eCommerce', 'One Page', 'Static', 'Dynamic', 'Web App']);
    $package = $faker->randomElement(['Silver', 'GOLD', 'Plutinum', 'Plutinum Plus']);
    $platform = $faker->randomElement(['Laravel', 'Row PHP', 'Wordpress', 'Vue']);
    return [
        'name' => $faker->unique()->domainWord,
        'domain' => $faker->unique()->domainName,
        'hosting' => $faker->url,
        'customer_name' => $faker->name,
        'type' => $type,
        'package' => $package,
        'platform' => $platform,
        'register_date' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'expire_date' => $faker->date($format = 'Y-m-d', $min = 'now'),
        'domain_expire_date' => $faker->date($format = 'Y-m-d', $min = 'now'),
        'website_login_url' => $faker->url,
        'website_username' => $faker->username,
        'website_password' => $faker->password,
        'note' => $faker->text($maxNbChars = 500),
    ];
});
