<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAllWebsitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('all_websites', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('domain')->nullable();
            $table->string('hosting')->nullable();
            $table->string('customer_name')->nullable();
            $table->string('type')->nullable();
            $table->string('package')->nullable();
            $table->string('platform')->nullable();
            $table->string('register_date')->nullable();
            $table->string('expire_date')->nullable();
            $table->string('domain_expire_date')->nullable();
            $table->string('website_login_url')->nullable();
            $table->string('website_username')->nullable();
            $table->string('website_password')->nullable();
            $table->tinyInteger('dev_status')->default('1');
            $table->text('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('all_websites');
    }
}
