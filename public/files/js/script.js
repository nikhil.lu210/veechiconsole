$(function(){
    $("#removeDisabled").click(function(){
        $('.removeDis').removeAttr('disabled', 'disabled');
        $('.removeDis').removeClass('disabled');
        $('#removeDisabled').addClass('d-none');
        $('.hiddenButton').removeClass('d-none');

    });

    $("#addDisabled").click(function(){
        $('.removeDis').attr('disabled', 'disabled');
        $('.removeDis').addClass('disabled');
        $('#removeDisabled').removeClass('d-none');
        $('.hiddenButton').addClass('d-none');

    });
});


$(function(){
    if($('.treeview').hasClass('is-expanded')){
        $('.treeview.is-expanded .app-menu__icon').removeClass('fa-folder');
        $('.treeview.is-expanded .app-menu__icon').addClass('fa-folder-open');
    }
});


$(function(){
    if($('a.treeview-item').hasClass('active')){
        $('a.treeview-item.active i').removeClass('fa-folder');
        $('a.treeview-item.active i').addClass('fa-folder-open');
    }
});