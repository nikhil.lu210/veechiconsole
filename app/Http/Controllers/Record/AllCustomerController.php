<?php

namespace App\Http\Controllers\Record;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Record\AllCustomer;

class AllCustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('record.customer.index')
                ->with('allcustomers', AllCustomer::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('record.customer.create')
                ->with('allcustomers', AllCustomer::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'customer_name' => 'required | string',
            'joining_date' => 'required',
            'mobile_number' => 'required',
            'email' => 'required',
            'status' => 'required',
        ));

        $allCustomer = new AllCustomer();

        $allCustomer->customer_name = $request->customer_name;
        $allCustomer->joining_date = $request->joining_date;
        $allCustomer->mobile_number = $request->mobile_number;
        $allCustomer->email = $request->email;
        $allCustomer->status = $request->status;
        $allCustomer->address = $request->address;
        $allCustomer->note = $request->note;

        $allCustomer->save();

        return redirect()->route('record.allcustomer.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('record.customer.show')
                ->with('allcustomers', AllCustomer::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, array(
            'customer_name' => 'required | string',
            'joining_date' => 'required',
            'mobile_number' => 'required',
            'email' => 'required',
            'status' => 'required',
        ));

        $allCustomer = AllCustomer::find($id);

        $allCustomer->customer_name = $request->customer_name;
        $allCustomer->joining_date = $request->joining_date;
        $allCustomer->mobile_number = $request->mobile_number;
        $allCustomer->email = $request->email;
        $allCustomer->status = $request->status;
        $allCustomer->address = $request->address;
        $allCustomer->note = $request->note;

        $allCustomer->save();

        return redirect()->route('record.allcustomer.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete_customer = AllCustomer::find($id);
        $delete_customer->delete();

        return redirect()->back();
    }
}
