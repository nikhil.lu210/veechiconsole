<?php

namespace App\Http\Controllers\Record;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Record\AllWebsite;
use App\Model\Setting\Domain;
use App\Model\Setting\Hosting;
use App\Model\Record\AllCustomer;
use App\Model\Setting\WebsiteType;
use App\Model\Setting\Package;
use App\Model\Setting\Platform;
use DB;

class AllWebsiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $domain = DB::table('domains')
                    ->select('domain')
                    ->get();
        $hosting = DB::table('hostings')
                    ->select('hosting')
                    ->get();
        $customer = DB::table('all_customers')
                    ->select('customer_name')
                    ->get();
        $websiteType = DB::table('website_types')
                    ->select('type')
                    ->get();
        $package = DB::table('packages')
                    ->select('package')
                    ->get();
        $platform = DB::table('platforms')
                    ->select('platform')
                    ->get();

        // $curDate = date('d-m-Y');
        // $exp = DB::table('all_websites')
        //                 ->select('expire_date')
        //                 ->get();
        // $curDate = strtotime(date('d-m-Y'));
        // $expDate = strtotime(date($exp));

        // dd($platform);
        return view('record.website.index')
            ->withDomains($domain)
            ->withHostings($hosting)
            ->withCustomers($customer)
            ->withWebsiteTypes($websiteType)
            ->withPackages($package)
            ->withPlatforms($platform)
            ->with('allwebsites', AllWebsite::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $domain = DB::table('domains')
                    ->select('domain')
                    ->get();
        $hosting = DB::table('hostings')
                    ->select('hosting')
                    ->get();
        $customer = DB::table('all_customers')
                    ->select('customer_name')
                    ->get();
        $websiteType = DB::table('website_types')
                    ->select('type')
                    ->get();
        $package = DB::table('packages')
                    ->select('package')
                    ->get();
        $platform = DB::table('platforms')
                    ->select('platform')
                    ->get();

        // dd($platform);
        return view('record.website.create')
            ->withDomains($domain)
            ->withHostings($hosting)
            ->withCustomers($customer)
            ->withWebsiteTypes($websiteType)
            ->withPackages($package)
            ->withPlatforms($platform)
            ->with('allwebsites', AllWebsite::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'name' => 'required | string',
        ));

        $allWebsite = new AllWebsite();



        $allWebsite->name = $request->name;
        $allWebsite->domain = $request->domain;
        $allWebsite->hosting = $request->hosting;
        $allWebsite->customer_name = $request->customer_name;
        $allWebsite->type = $request->type;
        $allWebsite->package = $request->package;
        $allWebsite->platform = $request->platform;
        $allWebsite->register_date = $request->register_date;
        $allWebsite->expire_date = $request->expire_date;
        $allWebsite->domain_expire_date = $request->domain_expire_date;
        $allWebsite->website_login_url = $request->website_login_url;
        $allWebsite->website_username = $request->website_username;
        $allWebsite->website_password = $request->website_password;
        $allWebsite->dev_status = $request->dev_status;
        $allWebsite->note = $request->note;

        $allWebsite->save();

        return redirect()->route('record.allwebsite.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $domain = DB::table('domains')
                    ->select('domain')
                    ->get();
        $hosting = DB::table('hostings')
                    ->select('hosting')
                    ->get();
        $customer = DB::table('all_customers')
                    ->select('customer_name')
                    ->get();
        $websiteType = DB::table('website_types')
                    ->select('type')
                    ->get();
        $package = DB::table('packages')
                    ->select('package')
                    ->get();
        $platform = DB::table('platforms')
                    ->select('platform')
                    ->get();

        // dd($platform);
        return view('record.website.show')
            ->withDomains($domain)
            ->withHostings($hosting)
            ->withCustomers($customer)
            ->withWebsiteTypes($websiteType)
            ->withPackages($package)
            ->withPlatforms($platform)
            ->with('allwebsites', AllWebsite::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, array(
            'name' => 'required | string',
        ));

        $allWebsite = AllWebsite::find($id);

        $allWebsite->name = $request->name;
        $allWebsite->domain = $request->domain;
        $allWebsite->hosting = $request->hosting;
        $allWebsite->customer_name = $request->customer_name;
        $allWebsite->type = $request->type;
        $allWebsite->package = $request->package;
        $allWebsite->platform = $request->platform;
        $allWebsite->register_date = $request->register_date;
        $allWebsite->expire_date = $request->expire_date;
        $allWebsite->domain_expire_date = $request->domain_expire_date;
        $allWebsite->website_login_url = $request->website_login_url;
        $allWebsite->website_username = $request->website_username;
        $allWebsite->website_password = $request->website_password;
        $allWebsite->dev_status = $request->dev_status;
        $allWebsite->note = $request->note;

        $allWebsite->save();

        return redirect()->route('record.allwebsite.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete_allwebsite = AllWebsite::find($id);
        $delete_allwebsite->delete();

        return redirect()->back();
    }


    // public function checkActive()
    // {

    // }
}
