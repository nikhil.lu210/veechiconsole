<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Model\Record\AllCustomer;
use App\Model\Record\AllWebsite;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home')
                ->with('allcustomers', AllCustomer::all())
                ->with('allwebsites', AllWebsite::all());
    }

    public function showChangePasswordForm(){
        return view('auth.changepassword');
    }
}
