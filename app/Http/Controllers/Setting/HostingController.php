<?php

namespace App\Http\Controllers\Setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Setting\Hosting;

class HostingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('setting.hosting.index')->with('hostings', Hosting::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('setting.hosting.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'hosting' => 'required | string',
            'hosting_login_url' => 'required',
            'hosting_username' => 'required',
            'hosting_password' => 'required',
        ));

        $hosting = new Hosting();

        $hosting->hosting = $request->hosting;
        $hosting->hosting_login_url = $request->hosting_login_url;
        $hosting->hosting_username = $request->hosting_username;
        $hosting->hosting_password = $request->hosting_password;
        $hosting->note = $request->note;

        $hosting->save();

        return redirect()->Route('setting.hosting.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('setting.hosting.show')->with('hostings', Hosting::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, array(
            'hosting' => 'required | string',
            'hosting_login_url' => 'required',
            'hosting_username' => 'required',
            'hosting_password' => 'required',
        ));

        $hosting = Hosting::find($id);

        $hosting->hosting = $request->hosting;
        $hosting->hosting_login_url = $request->hosting_login_url;
        $hosting->hosting_username = $request->hosting_username;
        $hosting->hosting_password = $request->hosting_password;
        $hosting->note = $request->note;

        $hosting->save();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete_hosting = Hosting::find($id);
        $delete_hosting->delete();

        return redirect()->back();
    }
}
