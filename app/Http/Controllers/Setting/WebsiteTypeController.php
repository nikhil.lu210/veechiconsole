<?php

namespace App\Http\Controllers\Setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Setting\WebsiteType;

class WebsiteTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $websiteTypes = WebsiteType::all();
        return view('setting.website_type.index')->with('websiteTypes', WebsiteType::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('setting.website_type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'type' => 'required | string',
        ));

        $websiteType = new WebsiteType();

        $websiteType->type = $request->type;
        $websiteType->basic_price = $request->basic_price;
        $websiteType->note = $request->note;

        $websiteType->save();

        return redirect()->Route('setting.website_type.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('setting.website_type.show')
            ->with('websiteTypes', WebsiteType::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, array(
            'type' => 'required | string',
        ));

        $websiteType = WebsiteType::find($id);

        $websiteType->type = $request->type;
        $websiteType->basic_price = $request->basic_price;
        $websiteType->note = $request->note;

        $websiteType->save();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete_website_type = WebsiteType::find($id);
        $delete_website_type->delete();

        return redirect()->back();

    }
}
