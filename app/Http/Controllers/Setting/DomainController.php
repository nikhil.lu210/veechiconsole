<?php

namespace App\Http\Controllers\Setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Setting\Domain;

class DomainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('setting.domain.index')->with('domains', Domain::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('setting.domain.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'domain' => 'required | string',
            'domain_login_url' => 'required',
            'domain_username' => 'required',
            'domain_password' => 'required',
        ));

        $domain = new Domain();

        $domain->domain = $request->domain;
        $domain->domain_login_url = $request->domain_login_url;
        $domain->domain_username = $request->domain_username;
        $domain->domain_password = $request->domain_password;
        $domain->note = $request->note;

        $domain->save();

        return redirect()->Route('setting.domain.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('setting.domain.show')->with('domains', Domain::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, array(
            'domain' => 'required | string',
            'domain_login_url' => 'required',
            'domain_username' => 'required',
            'domain_password' => 'required',
        ));

        $domain = Domain::find($id);

        $domain->domain = $request->domain;
        $domain->domain_login_url = $request->domain_login_url;
        $domain->domain_username = $request->domain_username;
        $domain->domain_password = $request->domain_password;
        $domain->note = $request->note;

        $domain->save();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete_domain = Domain::find($id);
        $delete_domain->delete();

        return redirect()->back();
    }
}
