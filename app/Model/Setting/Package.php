<?php

namespace App\Model\Setting;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $fillable =[
        'package',
        'basic_price',
        'note'
    ];
}
