<?php

namespace App\Model\Setting;

use Illuminate\Database\Eloquent\Model;

class Hosting extends Model
{
    protected $fillable = [
        'hosting',
        'hosting_login_url',
        'hosting_username',
        'hosting_password',
        'note'
    ];
}
