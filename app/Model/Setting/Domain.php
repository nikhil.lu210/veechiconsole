<?php

namespace App\Model\Setting;

use Illuminate\Database\Eloquent\Model;

class Domain extends Model
{
    protected $fillable = [
        'domain',
        'domain_login_url',
        'domain_username',
        'domain_password',
        'note'
    ];
}
