<?php

namespace App\Model\Setting;

use Illuminate\Database\Eloquent\Model;

class Platform extends Model
{
    protected $fillable =[
        'platform',
        'basic_price',
        'note'
    ];
}
