<?php

namespace App\Model\Setting;

use Illuminate\Database\Eloquent\Model;

class WebsiteType extends Model
{
    protected $fillable =[
        'type',
        'basic_price',
        'note'
    ];
}
