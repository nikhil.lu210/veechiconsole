<?php

namespace App\Model\Record;

use Illuminate\Database\Eloquent\Model;

class AllWebsite extends Model
{
    protected $fillable =[
        'name',
        'domain',
        'hosting',
        'customer_name',
        'type',
        'package',
        'platform',
        'register_date',
        'expire_date',
        'domain_expire_date',
        'website_login_url',
        'website_username',
        'website_password',
        'dev_status',
        'note'
    ];
}
