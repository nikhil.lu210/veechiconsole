<?php

namespace App\Model\Record;

use Illuminate\Database\Eloquent\Model;

class AllCustomer extends Model
{
    protected $fillable =[
        'customer_name',
        'joining_date',
        'mobile_number',
        'email',
        'status',
        'address',
        'note'
    ];
}
